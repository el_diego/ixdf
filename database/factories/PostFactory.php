<?php

namespace Database\Factories;

use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence($nbWords = 5, $variableNbWords = true),
            'body' => $this->faker->paragraphs($nbSentences = 8, $variableNbSentences = true)
        ];
    }

    /**
     * Set the model's author id.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function setAuthorId()
    {
        return $this->state(function (array $attributes) {
            return [
                'author_id' => $attributes['author_id'],
            ];
        });
    }
}
