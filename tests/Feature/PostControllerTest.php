<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PostControllerTest extends TestCase
{
    /**
     * @test
     * @return void
     */
    public function it_allows_anyone_to_see_list_all_posts()
    {
        $response = $this->get(route('get_all_posts'));
        $response->assertSuccessful();
        $response->assertViewIs('posts.index');
        $response->assertViewHas('posts');
    }
}
