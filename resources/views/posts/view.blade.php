@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                   <h1>{{$post->title}}</h1>
                   <p>
                    {{$post->body}}
                   </p>
                   @can('manage', $post)
                       <hr>
                       <a href="{{route('edit_post', ['id' => $post->id])}}" class="btn btn-info">Edit post</a>
                   @endcan
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
